FROM registry.gitlab.com/enki-portal/thermoengine:master
COPY Oxide-Geothermometer.ipynb ${HOME}
COPY GE08_test.xlsx ${HOME}
COPY jupyter_notebook_config.py ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
RUN pip install --no-cache-dir appmode==0.9.0 ipympl
RUN jupyter nbextension enable --py --sys-prefix appmode
RUN jupyter serverextension enable --py --sys-prefix appmode
USER ${NB_USER}
